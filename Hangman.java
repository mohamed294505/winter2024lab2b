import java.util.Scanner;
public class Hangman{
	
//	Prints out a message rompting the user for input calling the runGame method

	public static void main (String[] args){
		System.out.println(" Enter a four letter word to be guessed: ");
		Scanner input = new Scanner(System.in);
		String word = input.nextLine();
		runGame(word);

	}

//	Checks if the guessed letter in the given word
	public static int isLetterInWord(String word, char c){
		for(int i = 0; i < word.length(); i++){
			if(toUpperCase(c) == toUpperCase(word.charAt(i))){
				return i;
			}
		}
		return -1;
	}

//	Converts any given character into uppercase
	public static char toUpperCase(char c){
		return Character.toUpperCase(c);
	}
	
//	Prints out to the user the point there are in the game with correct and wrong guesses
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
		String display = "";
		boolean[] letters = {letter0, letter1, letter2, letter3};
		for(int i = 0; i < word.length(); i++){
			if(letters[i] == true){
				display = display + toUpperCase(word.charAt(i));
			} else{
				display = display + '_';
			}
		}
		System.out.println(display);
	}

//	Runs the game by calling the previous methods in a loop untill word is guessed or guesses run out
	public static void runGame(String word){
		Scanner input = new Scanner(System.in);
		boolean[] letters = {false, false, false, false};
		int misses = 0;
		
		while(misses < 6){
			
			System.out.println("\n Enter a letter: ");
			char guessedLetter = input.next().charAt(0);
			
			if(isLetterInWord(word, toUpperCase(guessedLetter)) > -1 && letters[isLetterInWord(word, toUpperCase(guessedLetter))] == false){
				letters[isLetterInWord(word, toUpperCase(guessedLetter))] = true;
				System.out.println("\n You gussed a letter correctly!");
			} else{
				misses++;
				System.out.println("\n Invalid guess, you have " + (6 - misses) + " left");
			}
			printWork(word, letters[0], letters[1], letters[2], letters[3]);
			if(letters[0] == true && letters[1] == true &&letters[2] == true && letters[3] == true){
				System.out.println("\n CONGRATULATIONS! You gussed the word correctly with " + misses + " misses!");
				break;
			}
		}
		if(misses == 6){
			System.out.println("\n YOU DIED");
		}
	}
}
