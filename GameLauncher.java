import java.util.Scanner;
public class GameLauncher{
	
	public static void main (String[] args){
		System.out.println("\u001B[36m" + " Welcome To THE GAME STATION!!!" + "\u001B[0m");
		System.out.println(" For Hangamen, please press 1");
		System.out.println(" For Wordle, please press 2");
		
		Scanner input = new Scanner(System.in);
		int choice = input.nextInt();
		input.nextLine();
		
		if (choice == 1){
			System.out.println("\u001B[31m" + " #HANGMAN#" + "\u001B[0m");
			System.out.println(" Enter a four letter word to be guessed: ");
			String word = input.nextLine();
			Hangman.runGame(word);
		}

		else if (choice == 2){
			System.out.println("\u001B[35m" + " #WORDLE#" + "\u001B[0m");
			Wordle.runGame(Wordle.generateWord());
		}
	}
}