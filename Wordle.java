import java.util.Random;
import java.util.Scanner;

public class Wordle{
	//Runs the game by calling the generateWord function withing calling the rungame function
	
	public static void main(String[] args) {
		runGame(generateWord());
	}
	
	//Picks a random word from an array of words to be guessed by the user
	public static String generateWord() {
		
	String[] words = new String[]{"BRICK", "PRICK", "CLIPT", "BRUNG", "BLING", "PLING", "CLUNK", "CHUNK", "JUMPY", "ALIVE", "AFTER", "FEAST", "ADMIT", "DANCE", "DREAM", "GHOST", "HEART", "IMAGE", "INDEX", "JUICE", "KNIFE", "MOUSE", "NORTH", "NAKED"};
	
	Random random = new Random();
	String targetWord = words[random.nextInt(words.length)];
	
	return targetWord;
	}
	
	//Checks if each letter of the guessed word is in the target word or not
	public static boolean letterInWord(String targetWord, char letter) {
		
		for(int i = 0; i < targetWord.length(); i++){
			if (Character.toUpperCase(letter) == targetWord.charAt(i)){
				return true;
			}
		}
		return false;
	}
	
	//Checks if each letter of the guessed word is in the correct slot of the target word or not
	public static boolean letterInSlot(String targetWord, char letter, int position) {
		if (Character.toUpperCase(letter) == targetWord.charAt(position)){
			return true;
		} 
		else{
			return false;
		}
	}
	
	//Creates an array of colors indicating how well the guess lines up with answer
	public static String[] guessWord(String answer, String guess) {
		
		String[] colors = new String[]{"white", "white", "white", "white", "white"};
		
		for(int i = 0; i < guess.length(); i++){
			if (letterInWord(answer, guess.charAt(i))){
				colors[i] = "yellow";
			}
			if (letterInSlot(answer, guess.charAt(i), i)){
				colors[i] = "green";
			}
		}
		return colors;
	}
	
	//Takes the previouse color array and prints the users guess with the corrosponding colors of the array
	public static void presentResults(String word, String[] colors) {
		
		for(int i = 0; i < word.length(); i++){
			if (colors[i].equals("white")){
				System.out.print("\u001B[0m" + Character.toUpperCase(word.charAt(i)) + "\u001B[0m");
			}
			if (colors[i].equals("yellow")){
				System.out.print("\u001B[33m" + Character.toUpperCase(word.charAt(i)) + "\u001B[0m");
			}
			if (colors[i].equals("green")){
				System.out.print("\u001B[32m" + Character.toUpperCase(word.charAt(i)) + "\u001B[0m");
			}
		}
		
	}
	
	//Prompts the user for a guess and checks if the guess is 5 letter long
	public static String readGuess() {
		System.out.println(" Please Enter a 5 Letter Word Guess: ");
		Scanner input = new Scanner(System.in);
		String userGuess = input.nextLine();
		
		while (userGuess.length() != 5){
			System.out.println("\u001B[31m" + "\n Invalid Guess >:(" + "\u001B[0m");
			System.out.println("\n Please Enter a 5 Letter Word Guess: ");
			userGuess = input.nextLine();
		}
		
		return userGuess.toUpperCase();
	}
	
	//Establishes the allowed attempts of the user, calls a chain of functions and prints a message depindong on the users guess, ends the program if the word is guessed correctly
	public static void runGame(String targetWord) {
		
		for(int guesses = 6; guesses > 0;){
			String userGuess = readGuess();
			presentResults(userGuess, guessWord(targetWord, userGuess));
			
			if (userGuess.equals(targetWord)){
				System.out.println("\u001B[32m" + "\n Gongratulations! You Have Guessed The Word Correctly!" + "\u001B[0m");
				break;
			}
			else {
				guesses--;
				System.out.println("\n You Have " + "\u001B[36m" + guesses + "\u001B[0m" + " Guesses Left");
			}
			if (guesses == 0){
				System.out.println("\u001B[31m" + "\n Better Luck Next time :(" + "\u001B[0m");
			}
		}
	}
}